// @ codekit-prepend "../../../node_modules/jquery/dist/jquery.min.js"
// @codekit-prepend "../../../node_modules/popper.js/dist/umd/popper.min.js"
// @codekit-prepend "../../../node_modules/bootstrap/dist/js/bootstrap.min.js"
// @codekit-prepend "../../../node_modules/autosize/dist/autosize.min.js"

/* global autosize, jQuery */

jQuery(document).ready(function ($) {
    // Autosize
    autosize($("textarea"));

    // Header
    $(window).scroll(function () {
        var header = $(".header");
        var headerOffset = header.innerHeight() / 2;

        if ($(window).scrollTop() > headerOffset) {
            // header.addClass('header--scroll animated slideInDown');
            header.addClass("header--scroll");
        } else {
            // header.removeClass('header--scroll animated slideInDown');
            header.removeClass("header--scroll");
        }
    });

    // Accordion
    $(".js-accordion-link").on("click", function (event) {
        event.preventDefault();

        var accordionItem = $(this).closest(".accordion-item");
        var accordionItemAnswerWrapper = accordionItem.find(
            ".accordion-item__answer-wrapper"
        );
        var accordionSiblings = accordionItem.siblings(".accordion-item");
        var accordionSiblingsAnswerWrapper = accordionSiblings.find(
            ".accordion-item__answer-wrapper"
        );

        if (!accordionItem.hasClass("accordion-item--open")) {
            var remDefault = 18;
            if (window.matchMedia("(max-width: 992px)").matches) {
                remDefault = 16;
            }

            accordionItem.addClass("accordion-item--open");
            accordionItemAnswerWrapper.css(
                "max-height",
                accordionItemAnswerWrapper[0].scrollHeight / remDefault +
                    24 / remDefault +
                    "rem"
            );
        } else {
            accordionItem.removeClass("accordion-item--open");
            accordionItemAnswerWrapper.css("max-height", 0);
        }

        accordionSiblings.removeClass("accordion-item--open");
        accordionSiblingsAnswerWrapper.css("max-height", 0);
    });

    // Process carousel
    $(".js-process-carousel").on("slide.bs.carousel", function (event) {
        var carouselIndicators = $(this).find(".carousel-indicators li");

        var carouselIndicatorsActive = carouselIndicators.filter(function (
            index
        ) {
            return index === event.to;
        });

        carouselIndicators.removeClass("prev-active next-active");

        carouselIndicatorsActive.prev().addClass("prev-active");
        carouselIndicatorsActive.next().addClass("next-active");
    });

    // Blocks
    function updateBlocks(wrapper) {
        wrapper = $(wrapper);
        var breakpoint = wrapper.data("blocks-breakpoint");
        var list = wrapper.find("[data-blocks-list]");
        var listWidth = list.width();
        var items = list.find("[data-blocks-item]").filter(".show");

        // Items per row
        var cols = wrapper.data("blocks-cols") * 1;
        if (window.matchMedia("(max-width: " + breakpoint + ")").matches) {
            cols = wrapper.data("blocks-breakpoint-cols") * 1;
        }

        var rows = Math.round(items.length / cols);
        var itemWidth = listWidth / cols;
        var itemHeight = null;
        var listHeight = null;
        var rowIndex = null;
        var columnIndex = null;
        var currentRowHeight = null;
        var currentItemHeight = null;
        var nextRowHeight = null;

        // Height of the list
        var itemHeightInput = wrapper.data("blocks-item-height");
        if (window.matchMedia("(max-width: " + breakpoint + ")").matches) {
            itemHeightInput = wrapper.data("blocks-breakpoint-item-height");
        }

        if (itemHeightInput === "auto") {
            rowIndex = 0;
            columnIndex = 0;

            currentRowHeight = 0;
            currentItemHeight = 0;

            items.each(function (index) {
                if (index !== 0 && index % cols === 0) {
                    rowIndex = rowIndex + 1;
                    columnIndex = 0;

                    listHeight = listHeight + currentRowHeight;
                    currentRowHeight = 0;
                    currentItemHeight = 0;
                }

                currentItemHeight = $(this).innerHeight();

                if (currentItemHeight > currentRowHeight) {
                    currentRowHeight = currentItemHeight;
                }

                columnIndex = columnIndex + 1;
            });

            listHeight = listHeight + currentRowHeight;
        } else {
            itemHeight = itemHeightInput * 1;
            listHeight = rows * itemHeight;
        }

        list.css("height", listHeight + "px");

        // Position the items
        if (itemHeightInput === "auto") {
            rowIndex = 0;
            columnIndex = 0;

            currentRowHeight = 0;
            currentItemHeight = 0;
            nextRowHeight = 0;

            items.each(function (index) {
                if (index !== 0 && index % cols === 0) {
                    rowIndex = rowIndex + 1;
                    columnIndex = 0;

                    nextRowHeight = nextRowHeight + currentRowHeight;
                    currentRowHeight = 0;
                    currentItemHeight = 0;
                }

                currentItemHeight = $(this).innerHeight();

                if (currentItemHeight > currentRowHeight) {
                    currentRowHeight = currentItemHeight;
                }

                $(this).css("top", nextRowHeight + "px");
                $(this).css("left", columnIndex * itemWidth + "px");

                columnIndex = columnIndex + 1;
            });
        } else {
            rowIndex = 0;
            columnIndex = 0;

            items.each(function (index) {
                if (index !== 0 && index % cols === 0) {
                    rowIndex = rowIndex + 1;
                    columnIndex = 0;
                }

                $(this).css("top", rowIndex * itemHeight + "px");
                $(this).css("left", columnIndex * itemWidth + "px");

                columnIndex = columnIndex + 1;
            });
        }
    }

    $("[data-blocks]").each(function () {
        updateBlocks(this);
    });

    $("[data-blocks-filter]").on("click", function () {
        var link = $(this);
        var linkTarget = link.attr("data-blocks-filter");
        var linkSiblings = link.siblings();
        var blocksSelector = '[data-blocks-item="' + linkTarget + '"]';

        if (linkTarget === "") {
            blocksSelector = "[data-blocks-item]";
        }

        var wrapper = link.closest("[data-blocks]");
        var allBlocks = wrapper.find("[data-blocks-item]");
        var showBlocks = wrapper.find(blocksSelector);
        var showingBlocks = showBlocks.filter(".hidden");
        var hiddenBlocks = allBlocks.not(showBlocks);
        var hidingBlocks = hiddenBlocks.filter(".show");

        linkSiblings.removeClass("active");
        link.addClass("active");

        hidingBlocks
            .removeClass("hiding hidden showing show")
            .addClass("hidden");
        showingBlocks
            .removeClass("hiding hidden showing show")
            .addClass("show");

        updateBlocks(wrapper);
    });

    $(window).resize(function () {
        $("[data-blocks]").each(function () {
            updateBlocks(this);
        });
    });

    // Eyecatcher carousel
    function updateCarouselControlsHeight(activeCarouselItem) {
        activeCarouselItem = $(activeCarouselItem);
        var activeCarouselCaption = activeCarouselItem
            .first()
            .find(".carousel-caption");
        var activeCarouselCaptionHeight = activeCarouselCaption.innerHeight();
        var carouselControlsWrapper = activeCarouselItem
            .closest(".carousel")
            .find(".carousel-controls-wrapper");

        carouselControlsWrapper.height(activeCarouselCaptionHeight);
    }

    function updateCarouselDependentColors(activeCarouselItem) {
        activeCarouselItem = $(activeCarouselItem);
        var activeDependentColor = activeCarouselItem
            .first()
            .data("dependent-color");
        var header = $(".header");

        header.removeClass(
            "header--dependent-color-default header--dependent-color-white"
        );
        header.addClass("header--dependent-color-" + activeDependentColor);
    }

    $(".eyecatcher .carousel").each(function () {
        var activeCarouselItem = $(this).find(".carousel-item.active");

        updateCarouselControlsHeight(activeCarouselItem);
        updateCarouselDependentColors(activeCarouselItem);
    });

    $(".eyecatcher .carousel").on("slid.bs.carousel", function (event) {
        var carouselItems = $(this).find(".carousel-item");

        var activeCarouselItem = carouselItems.filter(function (index) {
            return index === event.to;
        });

        updateCarouselControlsHeight(activeCarouselItem);
        updateCarouselDependentColors(activeCarouselItem);
    });
});

// Mobile Menu
/* eslint-disable */
function toggleMobileMenu() {
    jQuery("body").css(
        "overflow",
        !jQuery("#hamburger").is(":checked") ? "hidden" : "visible"
    );
}
/* eslint-enable */
