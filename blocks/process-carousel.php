<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'process-carousel';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$heading = get_field('heading');
$subheading = get_field('subheading');
$text = get_field('text');
$i = 0;
$link = get_field('button');

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="process-carousel__container">
        <div class="process-carousel__header-wrapper">
            <div class="process-carousel__header">

                <?php if (!empty($heading)): ?>
                    <h2 class="process-carousel__heading"><?php echo $heading; ?></h2>
                <?php endif; ?>

                <?php if (!empty($subheading)): ?>
                    <h3 class="process-carousel__subheading"><?php echo $subheading; ?></h3>
                <?php endif; ?>

                <?php if (!empty($text)): ?>
                    <p class="process-carousel__text"><?php echo $text; ?></p>
                <?php endif; ?>

            </div>
        </div>
    </div>

    <?php if (have_rows('items')): ?>
        <div class="process-carousel__carousel">
            <div id="<?php echo esc_attr($id); ?>_carousel" class="carousel slide js-process-carousel" data-ride="carousel" data-interval="false">
                <ol class="carousel-indicators">
                    <?php $i = 0; while (have_rows('items')): the_row(); ?>
                        <li data-target="#<?php echo esc_attr($id); ?>_carousel" data-slide-to="<?php echo $i; ?>" class="<?php echo $i == 0 ? 'active' : ''; ?> <?php echo $i == 1 ? 'next-active' : ''; ?>">
                            <?php echo the_sub_field('heading'); ?>
                        </li>
                    <?php $i++; endwhile; ?>
                </ol>

                <div class="carousel-inner">
                    <?php $i = 0; while (have_rows('items')): the_row(); ?>
                        <div class="carousel-item <?php echo $i == 0 ? 'active' : ''; ?>">
                            <div class="carousel-caption">
                                <div class="container">
                                    <div class="row justify-content-center">
                                        <div class="col-8">
                                            <div class="carousel-caption-inner">
                                                <?php the_sub_field('text'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php $i++; endwhile; ?>
                </div>

                <?php if ($i > 1): ?>
                    <div class="carousel-controls-wrapper">
                        <div class="container">
                            <div class="row justify-content-between">
                                <div class="col-2">
                                    <a class="carousel-control-prev" href="#<?php echo esc_attr($id); ?>_carousel" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                </div>
                                <div class="col-2">
                                    <a class="carousel-control-next" href="#<?php echo esc_attr($id); ?>_carousel" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if (!empty($link)): ?>
        <div class="process-carousel-footer">
            <div class="process-carousel-footer__container">

                <div class="process-carousel-footer__button-wrapper">
                    <a href="<?php echo esc_url($link['url']); ?>" class="process-carousel-footer__button" target="<?php echo esc_attr($link['target'] ?: '_self'); ?>"><?php echo esc_html($link['title']); ?></a>
                </div>

            </div>
        </div>
    <?php endif; ?>
</div>
