<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'cta-partner';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$image = get_field('image');
$image_retina = get_field('image_retina');
$heading = get_field('heading');
$text = get_field('text');

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
	<div class="cta-partner__container">

		<div class="cta-partner__content">

			<?php if (!empty($image)): ?>
                <div class="cta-partner__image-wrapper">
                    <img srcset="<?php echo esc_url($image['url']); ?> 1x<?php if (!empty($image_retina)): ?>, <?php echo esc_url($image_retina['url']); ?> 2x<?php endif; ?>" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" class="cta-partner__image">
                </div>
            <?php endif; ?>

			<div class="cta-partner__content-body">

				<?php if (!empty($heading)): ?>
					<h3 class="cta-partner__heading"><?php echo $heading; ?></h3>
				<?php endif; ?>

				<?php if (!empty($text)): ?>
					<p class="cta-partner__text"><?php echo $text; ?></p>
				<?php endif; ?>

				<?php if (have_rows('contact_points')): ?>
					<div class="cta-partner__contact-points">
						<?php while (have_rows('contact_points')): the_row(); ?>
							<div class="cta-partner__contact-point">

								<?php
	                                $image = get_sub_field('icon');
	                                $image_retina = get_sub_field('icon_retina');
	                                $image_width = get_sub_field('icon_width');
	                                $image_height = get_sub_field('icon_height');
	                            ?>
	                            <?php if (!empty($image)): ?>
	                                <div class="cta-partner__contact-point-icon-wrapper">
	                                    <img srcset="<?php echo esc_url($image['url']); ?> 1x<?php if (!empty($image_retina)): ?>, <?php echo esc_url($image_retina['url']); ?> 2x<?php endif; ?>" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" class="cta-partner__contact-point-icon" <?php echo !empty($image_width) ? 'width="'.$image_width.'"' : '' ?> <?php echo !empty($image_height) ? 'height="'.$image_height.'"' : '' ?>>
	                                </div>
	                            <?php endif; ?>

	                            <?php
	                                $value = get_sub_field('value');
	                                $url = get_sub_field('url');
	                            ?>
	                            <?php if (!empty($value)): ?>
									<div class="cta-partner__contact-point-value-wrapper">
										<span class="cta-partner__contact-point-value"><?php if (!empty($url)): ?><a href="<?php echo $url; ?>"><?php endif; ?><?php echo $value; ?><?php if (!empty($url)): ?></a><?php endif; ?></span>
									</div>
								<?php endif; ?>

							</div>
						<?php endwhile; ?>
					</div>
				<?php endif; ?>

			</div>

		</div>

	</div>
</div>
