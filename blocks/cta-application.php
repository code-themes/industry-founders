<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'cta-application';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$left = get_field('left');
$right = get_field('right');

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
	<div class="cta-application__container">

		<div class="cta-application__row align-items-end">

			<?php if (!empty($left['heading']) || !empty($left['text'])): ?>
				<div class="cta-application__col">
					<div class="cta-application-text cta-application__cta-application-text">

						<?php if (!empty($left['heading'])): ?>
							<h2 class="cta-application-text__heading"><?php echo $left['heading']; ?></h2>
						<?php endif; ?>

						<?php if (!empty($left['text'])): ?>
							<div class="cta-application-text__content">
								<?php echo $left['text']; ?>
							</div>
						<?php endif; ?>

					</div>
				</div>
			<?php endif; ?>

			<?php if (!empty($right['text']) || !empty($right['button'])): ?>
				<div class="cta-application__col">
					<div class="cta-application-button cta-application__cta-application-button">

						<?php if (!empty($right['text'])): ?>
							<div class="cta-application-button__content">
								<?php echo $right['text']; ?>
							</div>
						<?php endif; ?>

						<?php if (!empty($right['button'])): ?>
							<div class="cta-application-button__button-wrapper">
								<a href="<?php echo esc_url($right['button']['url']); ?>" class="cta-application-button__button" target="<?php echo esc_attr($right['button']['target'] ?: '_self'); ?>"><?php echo esc_html($right['button']['title']); ?></a>
							</div>
						<?php endif; ?>

					</div>
				</div>
			<?php endif; ?>

		</div>

	</div>
</div>
