<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'contact';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$text = get_field('text');
$people = get_field('people');
$form = get_field('form');

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
	<div class="contact__container">

		<div class="contact__row">
			<div class="contact__col">

				<?php if (!empty($text['heading']) || !empty($text['text'])): ?>
					<div class="contact-text">

						<?php if (!empty($text['heading'])): ?>
							<h2 class="contact-text__heading"><?php echo $text['heading']; ?></h2>
						<?php endif; ?>

						<?php if (!empty($text['text'])): ?>
							<div class="contact-text__content">
								<?php echo $text['text']; ?>
							</div>
						<?php endif; ?>

					</div>
				<?php endif; ?>

				<?php if (!empty($people['heading']) || !empty($people['items'])): ?>
					<div class="contact-people">

						<?php if (!empty($people['heading'])): ?>
							<h2 class="contact-people__heading"><?php echo $people['heading']; ?></h2>
						<?php endif; ?>

						<?php if (!empty($people['items'])): ?>
							<?php foreach ($people['items'] as $item): ?>
								<div class="contact-person contact-people__contact-person">
									
									<?php
										$image = $item['photo'];
										$image_retina = $item['photo_retina'];
									?>
									<?php if (!empty($image)): ?>
						                <div class="contact-person__photo-wrapper">
						                    <img srcset="<?php echo esc_url($image['url']); ?> 1x<?php if (!empty($image_retina)): ?>, <?php echo esc_url($image_retina['url']); ?> 2x<?php endif; ?>" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" class="contact-person__photo">
						                </div>
						            <?php endif; ?>

						            <?php if (!empty($item['text'])): ?>
										<div class="contact-person__body">
											<?php echo $item['text']; ?>
										</div>
									<?php endif; ?>

								</div>
							<?php endforeach; ?>
						<?php endif; ?>
					</div>
				<?php endif; ?>

			</div>

			<?php if (!empty($form)): ?>
				<div class="contact__col">
					<div class="contact-form">
						<?php echo str_replace(array(
	                        '<span class="wpcf7-form-control-wrap terms">', 
	                        '<span class="wpcf7-form-control-wrap privacy">', 
	                        'wpcf7-acceptance" aria-invalid="false"></span>',
	                        '<span class="wpcf7-form-control wpcf7-acceptance"><span class="wpcf7-list-item">',
	                        'name="terms" value="1" aria-invalid="false" /></span></span></span>',
	                        'name="privacy" value="1" aria-invalid="false" /></span></span></span>',
	                        'name="terms" value="1" aria-invalid="false" /></span></span>',
	                        'name="privacy" value="1" aria-invalid="false" /></span></span>'
	                    ), array(
	                        '', 
	                        '', 
	                        'wpcf7-acceptance" aria-invalid="false">',
	                        '',
	                        'name="terms" value="1" aria-invalid="false" />',
	                        'name="privacy" value="1" aria-invalid="false" />',
	                        'name="terms" value="1" aria-invalid="false" />',
	                        'name="privacy" value="1" aria-invalid="false" />'
	                    ), $form); ?>
	                </div>
				</div>
			<?php endif; ?>

		</div>

	</div>
</div>
