<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'pitches';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$heading = get_field('heading');
$subheading = get_field('subheading');

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>" data-blocks data-blocks-cols="2" data-blocks-item-height="auto" data-blocks-breakpoint="768px" data-blocks-breakpoint-cols="1" data-blocks-breakpoint-item-height="auto">
    <div class="pitches__container">

        <?php if (!empty($heading) || !empty($subheading)): ?>
            <div class="pitches__header">

                <?php if (!empty($heading)): ?>
                    <h2 class="pitches__heading"><?php echo $heading; ?></h2>
                <?php endif; ?>

                <?php if (!empty($subheading)): ?>
                    <h3 class="pitches__subheading"><?php echo $subheading; ?></h3>
                <?php endif; ?>

            </div>
        <?php endif; ?>

        <?php if (have_rows('filters')): ?>
            <div class="pitches__controls">
                <?php $i = 0; while (have_rows('filters')): the_row(); ?>
                    <button type="button" class="pitches__controls-button <?php echo $i == 0 ? 'active' : ''; ?>" data-blocks-filter="<?php the_sub_field('tag'); ?>"><?php the_sub_field('text'); ?></button>
                <?php $i++; endwhile; ?>
            </div>
        <?php endif; ?>

        <?php if (have_rows('items')): ?>
            <div class="pitches__blocks" data-blocks-list>
                <?php $i = 0; while (have_rows('items')): the_row(); ?>
                    <div class="pitches__block show" data-blocks-item="<?php the_sub_field('tag'); ?>">
                        <?php the_sub_field('text'); ?>
                    </div>
                <?php $i++; endwhile; ?>
            </div>
        <?php endif; ?>

    </div>
</div>
