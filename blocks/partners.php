<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'partners';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$heading = get_field('heading');
$subheading = get_field('subheading');
$text_left = get_field('text_left');
$text_right = get_field('text_right');
$cta = get_field('cta');

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="partners__container">

        <?php if (!empty($heading) || !empty($subheading)): ?>
            <div class="partners__header">

                <?php if (!empty($heading)): ?>
                    <h2 class="partners__heading"><?php echo $heading; ?></h2>
                <?php endif; ?>

                <?php if (!empty($subheading)): ?>
                    <h3 class="partners__subheading"><?php echo $subheading; ?></h3>
                <?php endif; ?>

            </div>
        <?php endif; ?>

        <?php if (!empty($text_left) || !empty($text_right)): ?>
            <div class="partners__text-wrapper">

                <?php if (!empty($text_left)): ?>
                    <div class="partners__text">
                        <?php echo $text_left; ?>
                    </div>
                <?php endif; ?>

                <?php if (!empty($text_right)): ?>
                    <div class="partners__text">
                        <?php echo $text_right; ?>
                    </div>
                <?php endif; ?>

            </div>
        <?php endif; ?>

        <div data-blocks data-blocks-cols="4" data-blocks-item-height="160" data-blocks-breakpoint="768px" data-blocks-breakpoint-cols="2" data-blocks-breakpoint-item-height="140">

            <?php if (have_rows('filters')): ?>
                <div class="partners__controls">
                    <?php $i = 0; while (have_rows('filters')): the_row(); ?>
                        <button type="button" class="partners__controls-button <?php echo $i == 0 ? 'active' : ''; ?>" data-blocks-filter="<?php the_sub_field('tag'); ?>"><?php the_sub_field('text'); ?></button>
                    <?php $i++; endwhile; ?>
                </div>
            <?php endif; ?>

            <?php if (have_rows('items')): ?>
                <div class="partners__logos" data-blocks-list>
                    <?php $i = 0; while (have_rows('items')): the_row(); ?>
                        <?php
                            $url = get_sub_field('url');
                            $image = get_sub_field('image');
                            $image_retina = get_sub_field('image_retina');
                        ?>
                        <div class="partners__logo-wrapper show" data-blocks-item="<?php the_sub_field('tag'); ?>">
                            <?php if (!empty($url)): ?><a href="<?php echo $url; ?>" class="partners__logo-link"><?php endif; ?><img srcset="<?php echo esc_url($image['url']); ?> 1x<?php if (!empty($image_retina)): ?>, <?php echo esc_url($image_retina['url']); ?> 2x<?php endif; ?>" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" class="partners__logo"><?php if (!empty($url)): ?></a><?php endif; ?>
                        </div>
                    <?php $i++; endwhile; ?>
                </div>
            <?php endif; ?>

        </div>

        <?php if (!empty($cta['heading']) || !empty($cta['text']) || !empty($cta['button']) || !empty($cta['image'])): ?>
            <div class="partners__cta">
                <div class="partners__cta-inner">
                    <div class="partners__cta-content">

                        <?php if (!empty($cta['heading'])): ?>
                            <h3 class="partners__cta-heading"><?php echo $cta['heading']; ?></h3>
                        <?php endif; ?>

                        <?php if (!empty($cta['text'])): ?>
                            <p class="partners__cta-text"><?php echo $cta['text']; ?></p>
                        <?php endif; ?>

                        <?php if (!empty($cta['button'])): ?>
                            <div class="partners__cta-button-wrapper">
                                <a href="<?php echo esc_url($cta['button']['url']); ?>" class="partners__cta-button" target="<?php echo esc_attr($cta['button']['target'] ?: '_self'); ?>"><?php echo esc_html($cta['button']['title']); ?></a>
                            </div>
                        <?php endif; ?>
                    </div>
                    <?php if (!empty($cta['image'])): ?>
                        <div class="partners__cta-image-wrapper">
                            <?php echo wp_get_attachment_image($cta['image']['id'], 'full', false, array('class' => 'partners__cta-image')); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        <?php endif; ?>

    </div>
</div>
