<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'process';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$heading = get_field('heading');
$subheading = get_field('subheading');
$text = get_field('text');

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
	<div class="process__container">

		<?php if (!empty($heading) || !empty($subheading) || !empty($text)): ?>
			<div class="process__header">
				<div class="process__header-row">
					<div class="process__header-col">

						<?php if (!empty($heading)): ?>
							<h2 class="process__heading"><?php echo $heading; ?></h2>
						<?php endif; ?>

						<?php if (!empty($subheading)): ?>
							<h3 class="process__subheading"><?php echo $subheading; ?></h3>
						<?php endif; ?>

						<?php if (!empty($text)): ?>
							<p class="process__text"><?php echo $text; ?></p>
						<?php endif; ?>

					</div>
				</div>
			</div>
		<?php endif; ?>

		<?php if (have_rows('items')): ?>
			<div class="process__steps">
				<div class="process__steps-row">
					<div class="process__steps-col">
						<?php $i = 0; while (have_rows('items')): the_row(); ?>
							<?php
								$heading = get_sub_field('heading');
								$icon = get_sub_field('icon');
								$text = get_sub_field('text');
							?>
							<div class="process-step">
								<div class="process-step__number-wrapper">
									<div class="process-step__number"><?php echo $i + 1; ?></div>
								</div>
								<div class="process-step__body">

									<?php if (!empty($heading)): ?>
										<div class="process-step__header">
											<h4 class="process-step__heading"><?php echo $heading; ?></h4>
										</div>
									<?php endif; ?>

									<?php if (!empty($icon) || !empty($text)): ?>
										<div class="process-step__content">

											<?php if (!empty($icon)): ?>
												<div class="process-step__icon-wrapper">
													<img src="<?php echo esc_url($icon['url']); ?>" alt="<?php echo esc_attr($icon['alt']); ?>" class="process-step__icon">
												</div>
											<?php endif; ?>

											<?php if (!empty($text)): ?>
												<div class="process-step__text-wrapper">
													<?php echo $text; ?>
												</div>
											<?php endif; ?>

										</div>
									<?php endif; ?>

								</div>
							</div>
						<?php $i++; endwhile; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>

	</div>
</div>
