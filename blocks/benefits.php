<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'benefits';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$heading = get_field('heading');
$subheading = get_field('subheading');
$text = get_field('text');

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="benefits__container">

        <div class="benefits__header">

            <?php if (!empty($heading)): ?>
                <h2 class="benefits__heading"><?php echo $heading; ?></h2>
            <?php endif; ?>

            <?php if (!empty($subheading)): ?>
                <h3 class="benefits__subheading"><?php echo $subheading; ?></h3>
            <?php endif; ?>

        </div>

        <?php if (!empty($text)): ?>
            <div class="benefits__text-wrapper">
                <div class="benefits__text">    
                    <?php echo $text; ?>
                </div>
            </div>
        <?php endif; ?>

        <?php if (have_rows('items')): ?>
            <div class="benefits__boxes">
                <?php while (have_rows('items')): the_row(); ?>
                    <div class="benefits__box-wrapper">
                        <div class="benefits__box">

                            <?php
                                $image = get_sub_field('icon');
                                $image_retina = get_sub_field('icon_retina');
                                $image_width = get_sub_field('icon_width');
                                $image_height = get_sub_field('icon_height');
                            ?>
                            <?php if (!empty($image)): ?>
                                <div class="benefits__box-image-wrapper">
                                    <img srcset="<?php echo esc_url($image['url']); ?> 1x<?php if (!empty($image_retina)): ?>, <?php echo esc_url($image_retina['url']); ?> 2x<?php endif; ?>" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" class="benefits__box-image" <?php echo !empty($image_width) ? 'width="'.$image_width.'"' : '' ?> <?php echo !empty($image_height) ? 'height="'.$image_height.'"' : '' ?>>
                                </div>
                            <?php endif; ?>
                            
                            <div class="benefits__box-body">

                                <?php $heading = get_sub_field('heading');
                                if (!empty($heading)): ?>
                                    <h4 class="benefits__box-heading"><?php echo $heading; ?></h4>
                                <?php endif; ?>

                                <?php $text = get_sub_field('text');
                                if (!empty($text)): ?>
                                    <p class="benefits__box-text"><?php echo $text; ?></p>
                                <?php endif; ?>

                            </div>
                            
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>

    </div>
</div>
