<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'faq';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$heading = get_field('heading');

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
	<div class="faq__container">

		<div class="faq__row">
			<div class="faq__col">

				<?php if (!empty($heading)): ?>
					<div class="faq__header">
						<h1 class="faq__heading"><?php echo $heading; ?></h1>
					</div>
				<?php endif; ?>

				<?php if (have_rows('items')): ?>
					<div class="faq__content">
						<div class="accordion">
							<?php while (have_rows('items')): the_row(); ?>
								<?php
									$question = get_sub_field('question');
									$answer = get_sub_field('answer');
								?>
								<div class="accordion-item">
									<button type="button" class="accordion-item__toggle js-accordion-link"></button>
									<div class="accordion-item__content">

										<?php if (!empty($question)): ?>
											<div class="accordion-item__question-wrapper">
												<h2 class="accordion-item__question"><?php echo $question; ?></h2>
											</div>
										<?php endif; ?>

										<?php if (!empty($answer)): ?>
											<div class="accordion-item__answer-wrapper">
												<?php echo $answer; ?>
											</div>
										<?php endif; ?>

									</div>
								</div>
							<?php endwhile; ?>
						</div>
					</div>
				<?php endif; ?>

			</div>
		</div>

	</div>
</div>
