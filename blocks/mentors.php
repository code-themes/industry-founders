<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'mentors';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$heading = get_field('heading');
$subheading = get_field('subheading');

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
	<div class="mentors__container">

		<?php if (!empty($heading) || !empty($subheading)): ?>
			<div class="mentors__header">

				<?php if (!empty($heading)): ?>
					<h2 class="mentors__heading"><?php echo $heading; ?></h2>
				<?php endif; ?>

				<?php if (!empty($subheading)): ?>
					<h3 class="mentors__subheading"><?php echo $subheading; ?></h3>
				<?php endif; ?>

			</div>
		<?php endif; ?>

		<?php if (have_rows('items')): ?>
			<div class="mentors__content">
				<?php while (have_rows('items')): the_row(); ?>
					<?php
						$image = get_sub_field('photo');
						$url = get_sub_field('url');
						$icon = get_sub_field('icon');
						$hashtag = get_sub_field('hashtag');
					?>
					<a <?php if (!empty($url)): ?>href="<?php echo esc_url($url); ?>" target="_blank"<?php endif; ?> class="mentors__link">

						<?php if (!empty($image)): ?>
							<span class="mentors__photo-wrapper">
			                    <?php echo wp_get_attachment_image($image['id'], 'full', false, array('class' => 'mentors__photo')); ?>
							</span>
		                <?php endif; ?>

		                <?php if (!empty($icon) || !empty($hashtag)): ?>
							<span class="mentors__info-wrapper">

								<?php if (!empty($icon)): ?>
									<span class="mentors__info-icon-wrapper"><img src="<?php echo esc_url($icon['url']); ?>" alt="<?php echo esc_attr($icon['alt']); ?>" class="mentors__info-icon"></span>
								<?php endif; ?>

								<?php if (!empty($hashtag)): ?>
									<span class="mentors__info-tag"><?php echo $hashtag; ?></span>
								<?php endif; ?>

							</span>
						<?php endif; ?>

					</a>
				<?php endwhile; ?>
			</div>
		<?php endif; ?>

	</div>
</div>
