<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'become-partner';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$heading = get_field('heading');
$subheading = get_field('subheading');
$text = get_field('text');

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
	<div class="become-partner__container">

		<?php if (!empty($heading) || !empty($subheading)): ?>
			<div class="become-partner__header">

				<?php if (!empty($heading)): ?>
					<h2 class="become-partner__heading"><?php echo $heading; ?></h2>
				<?php endif; ?>

				<?php if (!empty($subheading)): ?>
					<h3 class="become-partner__subheading"><?php echo $subheading; ?></h3>
				<?php endif; ?>

			</div>
		<?php endif; ?>

		<?php if (!empty($text)): ?>
			<div class="become-partner__text-wrapper">
				<div class="become-partner__text">
					<?php echo $text; ?>
				</div>
			</div>
		<?php endif; ?>

		<?php if (have_rows('items')): ?>
			<div class="become-partner__boxes">
				<?php while (have_rows('items')): the_row(); ?>
					<?php
						$type = get_sub_field('type');
						$text = get_sub_field('text');
						$image = get_sub_field('image');
					?>
					<div class="become-partner__box-wrapper">
						<div class="become-partner__box">
							<div class="become-partner__box-inner">

								<?php if ($type == 'text'): ?>
									<div class="become-partner__box-text-wrapper">

										<?php if (!empty($text['heading'])): ?>
											<h4 class="become-partner__box-heading"><?php echo $text['heading']; ?></h4>
										<?php endif; ?>

										<?php if (!empty($text['text'])): ?>
											<p class="become-partner__box-text"><?php echo $text['text']; ?></p>
										<?php endif; ?>

									</div>
								<?php endif; ?>

								<?php if ($type == 'image'): ?>
									<?php if (!empty($image['image'])): ?>
										<div class="become-partner__box-image-wrapper">
						                    <?php echo wp_get_attachment_image($image['image']['id'], 'full', false, array('class' => 'become-partner__box-image')); ?>
										</div>
					                <?php endif; ?>
					            <?php endif; ?>

							</div>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
		<?php endif; ?>

	</div>
</div>
