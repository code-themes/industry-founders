<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'cta-newsletter';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$left = get_field('left');
$right = get_field('right');

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
	<div class="cta-newsletter__container">

		<div class="cta-newsletter__row">

			<?php if (!empty($left['heading']) || !empty($left['text'])): ?>
				<div class="cta-newsletter__col cta-newsletter__col--text">
					<div class="cta-newsletter-text cta-newsletter__cta-newsletter-text">

						<?php if (!empty($left['heading'])): ?>
							<h2 class="cta-newsletter-text__heading"><?php echo $left['heading']; ?></h2>
						<?php endif; ?>

						<?php if (!empty($left['text'])): ?>
							<div class="cta-newsletter-text__content">
								<?php echo $left['text']; ?>
							</div>
						<?php endif; ?>

					</div>
				</div>
			<?php endif; ?>

			<div class="cta-newsletter__col cta-newsletter__col--form">

				<form class="cta-newsletter-form cta-newsletter__cta-newsletter-form" method="POST" action="<?php echo home_url(); ?>/?na=s" onsubmit="return newsletter_check(this)">
					<div class="cta-newsletter-form__form-group">
						<input type="text" name="nn" placeholder="Name" class="cta-newsletter-form__form-control" required>
					</div>
					<div class="cta-newsletter-form__form-group">
						<input type="text" name="ne" placeholder="E-Mail" class="cta-newsletter-form__form-control" required>
					</div>
					<?php if (!empty($right['checkbox_text'])): ?>
						<div class="cta-newsletter-form__form-group cta-newsletter-form__form-group--checkbox">
							<div class="custom-control custom-checkbox">
								<input type="checkbox" name="ny" class="custom-control-input" id="ctaNewsletterFormCheckbox" required>
								<label class="custom-control-label" for="ctaNewsletterFormCheckbox"><?php echo $right['checkbox_text']; ?></label>
							</div>
						</div>
					<?php endif; ?>
					<div class="cta-newsletter-form__form-submit">
						<button type="submit" class="cta-newsletter-form__button"><?php echo $right['button_text'] ?: 'Subscribe'; ?></button>
					</div>
				</form>
				
			</div>
		</div>

	</div>
</div>
