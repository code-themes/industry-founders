<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'heading';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$heading = get_field('heading');
$subheading = get_field('subheading');
$text = get_field('text');

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
	<div class="heading__container">

		<div class="heading__row">
			<div class="heading__col">

				<?php if (!empty($heading)): ?>
					<h1 class="heading__heading"><?php echo $heading; ?></h1>
				<?php endif; ?>

				<?php if (!empty($subheading)): ?>
					<h3 class="heading__subheading"><?php echo $subheading; ?></h3>
				<?php endif; ?>

				<?php if (!empty($text)): ?>
					<p class="heading__text"><?php echo $text; ?></p>
				<?php endif; ?>

			</div>
		</div>

	</div>
</div>
