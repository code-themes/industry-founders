<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'newsletter';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$text = get_field('text');
$button_text = get_field('button_text') ?: 'Subscribe';
$checkbox_text = get_field('checkbox_text');
$link = get_field('link');
$image = get_field('image');

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
	<div class="newsletter__container">

		<div class="newsletter__box">
			<div class="newsletter__box-inner">
				<div class="newsletter__text-wrapper">

					<?php if (!empty($text)): ?>
						<div class="newsletter__text">
							<?php echo $text; ?>
						</div>
					<?php endif; ?>

					<form class="newsletter__form" method="POST" action="<?php echo home_url(); ?>/?na=s" onsubmit="return newsletter_check(this)">
						<div class="newsletter__form-group">
							<input type="text" name="nn" placeholder="Name" class="newsletter__form-control" required>
						</div>
						<div class="newsletter__form-group">
							<input type="text" name="ne" placeholder="E-Mail" class="newsletter__form-control" required>
						</div>
						<?php if (!empty($checkbox_text)): ?>
							<div class="newsletter__form-group newsletter__form-group--checkbox">
								<div class="custom-control custom-checkbox">
									<input type="checkbox" name="ny" class="custom-control-input" id="newsletterFormCheckbox" required>
									<label class="custom-control-label" for="newsletterFormCheckbox"><?php echo $checkbox_text; ?></label>
								</div>
							</div>
						<?php endif; ?>
						<div class="newsletter__form-submit">
							<button type="submit" class="newsletter__button"><?php echo $button_text ?></button>
						</div>
						<?php if (!empty($link)): ?>
							<div class="newsletter__form-group newsletter__form-group--text">
								<a href="<?php echo esc_url($link['url']); ?>" class="newsletter__form-link" target="<?php echo esc_attr($link['target'] ?: '_self'); ?>"><?php echo esc_html($link['title']); ?></a>
							</div>
						<?php endif; ?>
					</form>
				</div>
				<?php if (!empty($image)): ?>
					<div class="newsletter__image-wrapper">
	                    <?php echo wp_get_attachment_image($image['id'], 'full', false, array('class' => 'newsletter__image')); ?>
					</div>
                <?php endif; ?>
			</div>
		</div>

	</div>
</div>
