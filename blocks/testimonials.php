<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'testimonials';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$heading = get_field('heading');
$subheading = get_field('subheading');

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
	<div class="testimonials__container">

		<?php if (!empty($heading) || !empty($subheading)): ?>
			<div class="testimonials__header">

				<?php if (!empty($heading)): ?>
					<h2 class="testimonials__heading"><?php echo $heading; ?></h2>
				<?php endif; ?>

				<?php if (!empty($subheading)): ?>
					<h3 class="testimonials__subheading"><?php echo $subheading; ?></h3>
				<?php endif; ?>

			</div>
		<?php endif; ?>

	</div>

	<?php if (have_rows('items')): ?>
		<div class="testimonials__carousel">
			<div id="<?php echo esc_attr($id); ?>_carousel" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
					<?php $i = 0; while (have_rows('items')): the_row(); ?>
						<li data-target="#<?php echo esc_attr($id); ?>_carousel" data-slide-to="<?php echo $i; ?>" class="<?php echo $i == 0 ? 'active' : ''; ?>"></li>
					<?php $i++; endwhile; ?>
				</ol>
				<div class="carousel-inner">
					<?php $i = 0; while (have_rows('items')): the_row(); ?>
						<div class="carousel-item <?php echo $i == 0 ? 'active' : ''; ?>">
							<div class="carousel-caption">
								<div class="testimonial">
									<div class="testimonial__container">
										<div class="testimonial__row">
											<div class="testimonial__col">

												<?php
					                                $image = get_sub_field('photo');
					                                $image_retina = get_sub_field('photo_retina');
					                            ?>
					                            <?php if (!empty($image)): ?>
					                                <div class="testimonial__photo-wrapper">
					                                    <img srcset="<?php echo esc_url($image['url']); ?> 1x<?php if (!empty($image_retina)): ?>, <?php echo esc_url($image_retina['url']); ?> 2x<?php endif; ?>" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" class="testimonial__photo">
					                                </div>
					                            <?php endif; ?>

					                            <?php
					                            	$text = get_sub_field('text');
					                            	$name = get_sub_field('name');
					                            ?>
					                            <?php if (!empty($text) || !empty($name)): ?>
													<blockquote class="testimonial__content">

														<?php if (!empty($text)): ?>
															<p class="testimonial__text"><?php echo $text; ?></p>
														<?php endif; ?>

														<?php if (!empty($name)): ?>
															<footer class="testimonial__name"><?php echo $name; ?></footer>
														<?php endif; ?>

													</blockquote>
												<?php endif; ?>

											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php $i++; endwhile; ?>
				</div>
				<?php if ($i > 1): ?>
					<div class="carousel-controls-wrapper">
						<div class="container">
							<div class="row justify-content-between">
								<div class="col-1">
									<a class="carousel-control-prev" href="#<?php echo esc_attr($id); ?>_carousel" role="button" data-slide="prev">
										<span class="carousel-control-prev-icon" aria-hidden="true"></span>
										<span class="sr-only">Previous</span>
									</a>
								</div>
								<div class="col-1">
									<a class="carousel-control-next" href="#<?php echo esc_attr($id); ?>_carousel" role="button" data-slide="next">
										<span class="carousel-control-next-icon" aria-hidden="true"></span>
										<span class="sr-only">Next</span>
									</a>
								</div>
							</div>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	<?php endif; ?>
</div>
