<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'benefits-2';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$heading = get_field('heading');
$subheading = get_field('subheading');
$text_left = get_field('text_left');
$text_right = get_field('text_right');

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
	<div class="benefits-2__container">

		<?php if (!empty($heading) || !empty($subheading)): ?>
			<div class="benefits-2__header">

				<?php if (!empty($heading)): ?>
					<h2 class="benefits-2__heading"><?php echo $heading; ?></h2>
				<?php endif; ?>

				<?php if (!empty($subheading)): ?>
					<h3 class="benefits-2__subheading"><?php echo $subheading; ?></h3>
				<?php endif; ?>

			</div>
		<?php endif; ?>

		<?php if (!empty($text_left) || !empty($text_right)): ?>
			<div class="benefits-2__text-wrapper">

				<?php if (!empty($text_left)): ?>
					<div class="benefits-2__text">
						<?php echo $text_left; ?>
					</div>
				<?php endif; ?>

				<?php if (!empty($text_right)): ?>
					<div class="benefits-2__text">
						<?php echo $text_right; ?>
					</div>
				<?php endif; ?>

			</div>
		<?php endif; ?>

		<?php if (have_rows('items')): ?>
			<div class="benefits-2__boxes">
				<?php while (have_rows('items')): the_row(); ?>
					<?php
						$type = get_sub_field('type');
						$text = get_sub_field('text');
						$image = get_sub_field('image');
					?>
					<div class="benefits-2__box-wrapper">
						<div class="benefits-2__box">
							<div class="benefits-2__box-inner">

								<?php if ($type == 'text'): ?>
									<div class="benefits-2__box-text-wrapper">

										<?php if (!empty($text['heading'])): ?>
											<h4 class="benefits-2__box-heading"><?php echo $text['heading']; ?></h4>
										<?php endif; ?>

										<?php if (!empty($text['text'])): ?>
											<p class="benefits-2__box-text"><?php echo $text['text']; ?></p>
										<?php endif; ?>

									</div>
								<?php endif; ?>

								<?php if ($type == 'image'): ?>
									<?php if (!empty($image['image'])): ?>
										<div class="benefits-2__box-image-wrapper">
						                    <?php echo wp_get_attachment_image($image['image']['id'], 'full', false, array('class' => 'benefits-2__box-image')); ?>
										</div>
					                <?php endif; ?>
					            <?php endif; ?>

							</div>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
		<?php endif; ?>

	</div>
</div>
