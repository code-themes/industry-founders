<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'eyecatcher';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$orientation = get_field('orientation');

$i = 0;

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?> eyecatcher--orientation-<?php echo $orientation ?>">
    <div id="<?php echo esc_attr($id); ?>_carousel" class="carousel slide" data-ride="carousel">
        
        <?php if (have_rows('items')): ?>
            <div class="carousel-inner">
                <?php $i = 0; while (have_rows('items')): the_row(); ?>
                    <div class="carousel-item <?php echo $i == 0 ? 'active' : ''; ?>" data-dependent-color="<?php the_sub_field('dependent_color'); ?>">
                        
                        <?php $image = get_sub_field('image');
                        if (!empty($image)): ?>
                            <?php echo wp_get_attachment_image($image['id'], 'full'); ?>
                        <?php endif; ?>

                        <?php
                            $heading = get_sub_field('heading');
                            $subheading = get_sub_field('subheading');
                            $text = get_sub_field('text');
                        ?>
                        <?php if (!empty($heading) || !empty($subheading) || !empty($text)): ?>
                            <div class="carousel-caption">
                                <div class="container">
                                    <div class="row justify-content-center">
                                        <div class="col-10 clear-content-margins">
                                            <?php if (!empty($heading)): ?>
                                                <h1><?php echo $heading; ?></h1>
                                            <?php endif; ?>
                                            <?php if (!empty($subheading)): ?>
                                                <h2><?php echo $subheading; ?></h2>
                                            <?php endif; ?>
                                            <?php if (!empty($text)): ?>
                                                <p><?php echo $text; ?></p>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>

                    </div>
                <?php $i++; endwhile; ?>
            </div>
        <?php endif; ?>

        <?php if ($i > 1): ?>
            <div class="carousel-controls-wrapper">
                <div class="container">
                    <div class="row justify-content-between">
                        <div class="col-1">
                            <a class="carousel-control-prev" href="#<?php echo esc_attr($id); ?>_carousel" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                        </div>
                        <div class="col-1">
                            <a class="carousel-control-next" href="#<?php echo esc_attr($id); ?>_carousel" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

    </div>
</div>