<?php

/**
 * Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'text';
if (!empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// Load values and assign defaults.
$heading = get_field('heading');
$subheading = get_field('subheading');
$text_left = get_field('text_left');
$text_right = get_field('text_right');

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
	<div class="text__container">

		<?php if (!empty($heading) || !empty($subheading)): ?>
			<div class="text__header">

				<?php if (!empty($heading)): ?>
					<h2 class="text__heading"><?php echo $heading; ?></h2>
				<?php endif; ?>

				<?php if (!empty($subheading)): ?>
					<h3 class="text__subheading"><?php echo $subheading; ?></h3>
				<?php endif; ?>

			</div>
		<?php endif; ?>

		<?php if (!empty($text_left) || !empty($text_right)): ?>
			<div class="text__row">

				<?php if (!empty($text_left)): ?>
					<div class="text__col">
						<?php echo $text_left; ?>
					</div>
				<?php endif; ?>

				<?php if (!empty($text_right)): ?>
					<div class="text__col">
						<?php echo $text_right; ?>
					</div>
				<?php endif; ?>

			</div>
		<?php endif; ?>

	</div>
</div>
