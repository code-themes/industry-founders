            </div>
            
            <footer class="footer" role="contentinfo" itemscope itemtype="https://schema.org/WPFooter">
                <div class="footer__container">

                    <div class="footer__links-copyright-wrapper">
                        <div class="footer__links-wrapper">

                            <nav class="footer__links">
                                <?php // see all default args here: https://developer.wordpress.org/reference/functions/wp_nav_menu/ ?>
                                <?php wp_nav_menu(array(
                                    'menu' => __('Footer Links', 'platetheme'),
                                    'menu_class' => '',
                                    'container' => false,
                                    'container_class' => '',
                                    'depth' => 1,
                                    'theme_location' => 'footer-links'.get_current_page_language('-'),
                                )); ?>
                            </nav>

                            <?php if (have_rows('social_links', 'option')): ?>
                                <nav class="footer__social-links">
                                    <ul>
                                        <?php while (have_rows('social_links', 'option')): the_row(); ?>
                                            <li class="<?php the_sub_field('icon'); ?>">
                                                <?php $social_link = get_sub_field('link'); ?>
                                                <a href="<?php echo esc_url($social_link['url']); ?>" target="<?php echo esc_attr($social_link['target'] ?: '_self'); ?>"><?php echo esc_html($social_link['title']); ?></a>
                                            </li>
                                        <?php endwhile; ?>
                                    </ul>
                                </nav>
                            <?php endif; ?>

                        </div>
                        <div class="footer__copyright-wrapper">
                            <div class="footer__copyright">
                                <span class="footer__text-name">&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?></span>
                                <a href="https://upstart.de/" target="_blank" class="footer__text-by">by UPSTART</a>
                            </div>
                        </div>
                    </div>

                </div>
            </footer>

        </div>

		<?php // all js scripts are loaded in library/functions.php ?>
		<?php wp_footer(); ?>

        <?php echo get_field('before_the_closing_body_tag', 'option'); ?>

	</body>
</html>
