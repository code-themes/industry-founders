<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php html_schema(); ?> <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
	<head>

        <?php /**
         * updated with non-blocking order
         * see here: https://csswizardry.com/2018/11/css-and-network-performance/
         * 
         * In short, place any js here that doesn't need to act on css before any css to
         * speed up page loads.
         */
        ?>

        <?php echo get_field('after_the_opening_head_tag', 'option'); ?>

        <?php // See everything you need to know about the <head> here: https://github.com/joshbuchea/HEAD ?>
        <meta charset='<?php bloginfo( 'charset' ); ?>'>
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <?php
            // Images > Favicon
            // for more: http://www.jonathantneal.com/blog/understand-the-favicon/
            $favicon_png = get_field('images_favicon_png', 'option') ?: get_theme_file_uri().'/favicon.png';
            $favicon_ico = get_field('images_favicon_ico', 'option') ?: get_theme_file_uri().'/favicon.ico';
        ?>
        <link rel="icon" href="<?php echo $favicon_png; ?>">
        <!--[if IE]>
            <link rel="shortcut icon" href="<?php echo $favicon_ico; ?>">
        <![endif]-->

        <?php
            // Images > Apple Touch Icon
            $apple_touch_icon = get_field('images_apple_touch_icon', 'option') ?: get_theme_file_uri().'/library/images/apple-touch-icon.png';
        ?>
        <link rel="apple-touch-icon" href="<?php echo $apple_touch_icon; ?>">

        <?php // updated pingback. Thanks @HardeepAsrani https://github.com/HardeepAsrani ?>
        <?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
            <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <?php endif; ?>

        <?php // put font scripts like Typekit/Adobe Fonts here ?>
        <?php // end fonts ?>

        <?php // wordpress head functions ?>
        <?php wp_head(); ?>
        <?php // end of wordpress head ?>

        <?php
            // Colors
            $body_bg = get_field('vars_body_bg', 'option') ?: '#ffffff';
            $body_color = get_field('vars_body_color', 'option') ?: '#000000';

            $primary = get_field('vars_primary', 'option') ?: '#130F4B';
            $secondary = get_field('vars_secondary', 'option') ?: '#6DFF37';

            $primary_button = get_field('vars_primary_button', 'option');
            $secondary_button = get_field('vars_secondary_button', 'option');
        ?>
        <style type="text/css">
            :root {
                --body-bg: <?php echo $body_bg; ?>;
                --body-color: <?php echo $body_color; ?>;

                --primary: <?php echo $primary; ?>;
                --secondary: <?php echo $secondary; ?>;
            }

            .eyecatcher .carousel-caption {
                background: <?php echo hex2rgba($primary, 0.9); ?>;
            }

            .mentors__info-wrapper {
                background: <?php echo hex2rgba($secondary, 0.9); ?>;
            }

            .btn-primary, .main-menu>ul>li.main-menu__list-item--button>a, .partners__controls-button, .pitches__controls-button {
                background-color: <?php echo !empty($primary_button['background']) ? $primary_button['background'] :  $primary; ?>;
                border-color: <?php echo !empty($primary_button['background']) ? $primary_button['background'] : $primary; ?>;

                <?php if (!empty($primary_button['color'])): ?>
                    color: <?php echo $primary_button['color']; ?> !important;
                <?php endif; ?>
            }

            .btn-primary:hover, .main-menu>ul>li.main-menu__list-item--button>a:hover, .partners__controls-button:hover, .pitches__controls-button:hover, .btn-primary:not(:disabled):not(.disabled):active, .main-menu>ul>li.main-menu__list-item--button>a:not(:disabled):not(.disabled):active, .partners__controls-button:not(:disabled):not(.disabled):active, .pitches__controls-button:not(:disabled):not(.disabled):active, .btn-primary:not(:disabled):not(.disabled).active, .main-menu>ul>li.main-menu__list-item--button>a:not(:disabled):not(.disabled).active, :not(:disabled):not(.disabled).active.partners__controls-button, :not(:disabled):not(.disabled).active.pitches__controls-button {
                background-color: <?php echo !empty($primary_button['background']) ? $primary_button['background'] : $primary; ?>;
                border-color: <?php echo !empty($primary_button['background']) ? $primary_button['background'] : $primary; ?>;

                <?php if (!empty($primary_button['color'])): ?>
                    color: <?php echo $primary_button['color']; ?> !important;
                <?php endif; ?>
            }

            .btn-secondary, .contact-form__button, .cta-application-button__button, .cta-newsletter-form__button, .newsletter__button, .partners__cta-button, .process-carousel-footer__button, .teaser__button {
                background-color: <?php echo !empty($secondary_button['background']) ? $secondary_button['background'] : $secondary; ?>;
                border-color: <?php echo !empty($secondary_button['background']) ? $secondary_button['background'] : $secondary; ?>;

                <?php if (!empty($secondary_button['color'])): ?>
                    color: <?php echo $secondary_button['color']; ?> !important;
                <?php endif; ?>
            }

            .btn-secondary:hover, .contact-form__button:hover, .cta-application-button__button:hover, .cta-newsletter-form__button:hover, .newsletter__button:hover, .partners__cta-button:hover, .process-carousel-footer__button:hover, .teaser__button:hover, .btn-secondary:not(:disabled):not(.disabled):active, .contact-form__button:not(:disabled):not(.disabled):active, .cta-application-button__button:not(:disabled):not(.disabled):active, .cta-newsletter-form__button:not(:disabled):not(.disabled):active, .newsletter__button:not(:disabled):not(.disabled):active, .partners__cta-button:not(:disabled):not(.disabled):active, .process-carousel-footer__button:not(:disabled):not(.disabled):active, .teaser__button:not(:disabled):not(.disabled):active, .btn-secondary:not(:disabled):not(.disabled).active, :not(:disabled):not(.disabled).active.contact-form__button, :not(:disabled):not(.disabled).active.cta-application-button__button, :not(:disabled):not(.disabled).active.cta-newsletter-form__button, :not(:disabled):not(.disabled).active.newsletter__button, :not(:disabled):not(.disabled).active.partners__cta-button, :not(:disabled):not(.disabled).active.process-carousel-footer__button, :not(:disabled):not(.disabled).active.teaser__button {
                background-color: <?php echo !empty($secondary_button['background']) ? $secondary_button['background'] : $secondary; ?>;
                border-color: <?php echo !empty($secondary_button['background']) ? $secondary_button['background'] : $secondary; ?>;

                <?php if (!empty($secondary_button['color'])): ?>
                    color: <?php echo $secondary_button['color']; ?> !important;
                <?php endif; ?>
            }

            .partners__controls-button.active:not(:disabled):not(.disabled):active, .partners__controls-button.active:not(:disabled):not(.disabled).active, .show>.partners__controls-button.active.dropdown-toggle {
                background-color: <?php echo !empty($secondary_button['background']) ? $secondary_button['background'] : $secondary; ?>;
                border-color: <?php echo !empty($secondary_button['background']) ? $secondary_button['background'] : $secondary; ?>;

                <?php if (!empty($secondary_button['color'])): ?>
                    color: <?php echo $secondary_button['color']; ?> !important;
                <?php endif; ?>
            }

            .pitches__controls-button.active:not(:disabled):not(.disabled):active, .pitches__controls-button.active:not(:disabled):not(.disabled).active, .show>.pitches__controls-button.active.dropdown-toggle {
                background-color: <?php echo !empty($secondary_button['background']) ? $secondary_button['background'] : $secondary; ?>;
                border-color: <?php echo !empty($secondary_button['background']) ? $secondary_button['background'] : $secondary; ?>;

                <?php if (!empty($secondary_button['color'])): ?>
                    color: <?php echo $secondary_button['color']; ?> !important;
                <?php endif; ?>
            }

            .form-control:focus, .contact-form__form-control:focus, .cta-newsletter-form__form-control:focus, .newsletter__form-control:focus {
                border-color: <?php echo color_luminance($secondary, 0.25); ?>;
                box-shadow: 0 0 0 0.2rem <?php echo hex2rgba($secondary, 0.25); ?>;
            }

            .custom-control-input:focus ~ .custom-control-label::before {
                box-shadow: 0 0 0 0.2rem <?php echo hex2rgba($secondary, 0.25); ?>;
            }
        </style>

        <?php
            // Fonts
            $font_family_sans_serif = (get_field('vars_font_family_sans_serif', 'option') ?: 'Roboto').', sans-serif';
            $font_family_sans_serif_alt = (get_field('vars_font_family_sans_serif_alt', 'option') ?: 'Oswald').', sans-serif';
            $font_family_serif = (get_field('vars_font_family_serif', 'option') ?: '"PT Serif"').', serif';
        ?>
        <style type="text/css">
            :root {
                --font-family-sans-serif: <?php echo $font_family_sans_serif; ?>;
                --font-family-sans-serif-alt: <?php echo $font_family_sans_serif_alt; ?>;
                --font-family-serif: <?php echo $font_family_serif; ?>;
            }
        </style>

        <?php
            // Header
            $header_background = get_field('header_background', 'option');
            $logo_monochrome = get_field('images_logo_monochrome', 'option');
            $logo_monochrome_retina = get_field('images_logo_monochrome_retina', 'option');
            $logo = get_field('images_logo', 'option');
            $logo_retina = get_field('images_logo_retina', 'option');
            $logo_white = get_field('images_logo_white', 'option');
            $logo_white_retina = get_field('images_logo_white_retina', 'option');
            $logo_mobile_nav = get_field('images_logo_mobile_nav', 'option');
            $logo_mobile_nav_retina = get_field('images_logo_mobile_nav_retina', 'option');
            $header_menu = get_field('header_menu', 'option');
        ?>
        <?php if (!empty($header_background) || !empty($logo_monochrome) || !empty($logo) || !empty($logo_white) || !empty($logo_mobile_nav) || !empty($header_menu)): ?>
            <style type="text/css">
                <?php if (!empty($header_background)): ?>
                    .header {
                        background: <?php echo $header_background; ?>;
                    }
                <?php endif; ?>

                <?php if (!empty($logo_monochrome)): ?>
                    .logo {
                        background-size: initial;
                        background-image: url(<?php echo $logo_monochrome['url']; ?>);
                        <?php if ($logo_monochrome['mime_type'] != 'image/svg+xml'): ?>
                            height: <?php echo $logo_monochrome['height']; ?>px;
                            width: <?php echo $logo_monochrome['width']; ?>px;
                        <?php endif; ?>
                    }

                    <?php if (!empty($logo_monochrome_retina)): ?>
                        @media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {
                            .logo {
                                background-image: url(<?php echo $logo_monochrome_retina['url']; ?>);
                                <?php if ($logo_monochrome['mime_type'] != 'image/svg+xml'): ?>
                                    background-size: <?php echo $logo_monochrome['width']; ?>px <?php echo $logo_monochrome['height']; ?>px;
                                <?php endif; ?>
                            }
                        }
                    <?php endif; ?>
                <?php endif; ?>

                <?php if (!empty($logo)): ?>
                    .logo::after {
                        background-size: initial;
                        background-image: url(<?php echo $logo['url']; ?>);
                        <?php if ($logo['mime_type'] != 'image/svg+xml'): ?>
                            height: <?php echo $logo['height']; ?>px;
                            width: <?php echo $logo['width']; ?>px;
                        <?php endif; ?>
                    }

                    <?php if (!empty($logo_retina)): ?>
                        @media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {
                            .logo::after {
                                background-image: url(<?php echo $logo_retina['url']; ?>);
                                <?php if ($logo['mime_type'] != 'image/svg+xml'): ?>
                                    background-size: <?php echo $logo['width']; ?>px <?php echo $logo['height']; ?>px;
                                <?php endif; ?>
                            }
                        }
                    <?php endif; ?>
                <?php endif; ?>

                <?php if (!empty($logo_white)): ?>
                    .header--dependent-color-white .header__logo {
                        background-size: initial;
                        background-image: url(<?php echo $logo_white['url']; ?>);
                        <?php if ($logo_white['mime_type'] != 'image/svg+xml'): ?>
                            height: <?php echo $logo_white['height']; ?>px;
                            width: <?php echo $logo_white['width']; ?>px;
                        <?php endif; ?>
                    }

                    <?php if (!empty($logo_white_retina)): ?>
                        @media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {
                            .header--dependent-color-white .header__logo {
                                background-image: url(<?php echo $logo_white_retina['url']; ?>);
                                <?php if ($logo_white['mime_type'] != 'image/svg+xml'): ?>
                                    background-size: <?php echo $logo_white['width']; ?>px <?php echo $logo_white['height']; ?>px;
                                <?php endif; ?>
                            }
                        }
                    <?php endif; ?>
                <?php endif; ?>

                <?php if (!empty($logo)): ?>
                    .header--scroll .header__logo {
                        background-size: initial;
                        background-image: url(<?php echo $logo['url']; ?>);
                        <?php if ($logo['mime_type'] != 'image/svg+xml'): ?>
                            height: <?php echo $logo['height']; ?>px;
                            width: <?php echo $logo['width']; ?>px;
                        <?php endif; ?>
                    }

                    <?php if (!empty($logo_retina)): ?>
                        @media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {
                            .header--scroll .header__logo {
                                background-image: url(<?php echo $logo_retina['url']; ?>);
                                <?php if ($logo['mime_type'] != 'image/svg+xml'): ?>
                                    background-size: <?php echo $logo['width']; ?>px <?php echo $logo['height']; ?>px;
                                <?php endif; ?>
                            }
                        }
                    <?php endif; ?>
                <?php endif; ?>

                <?php if (!empty($logo)): ?>
                    @media (max-width: 991.98px) {
                        .header .header__logo {
                            background-size: initial;
                            background-image: url(<?php echo $logo['url']; ?>);
                            <?php if ($logo['mime_type'] != 'image/svg+xml'): ?>
                                height: <?php echo $logo['height']; ?>px;
                                width: <?php echo $logo['width']; ?>px;
                            <?php endif; ?>
                        }
                    }

                    <?php if (!empty($logo_retina)): ?>
                        @media only screen and (-webkit-min-device-pixel-ratio: 2) and (max-width: 991.98px), only screen and (max-width: 991.98px) and (min-resolution: 192dpi), only screen and (max-width: 991.98px) and (min-resolution: 2dppx) {
                            .header .header__logo {
                                background-image: url(<?php echo $logo_retina['url']; ?>);
                                <?php if ($logo['mime_type'] != 'image/svg+xml'): ?>
                                    background-size: <?php echo $logo['width']; ?>px <?php echo $logo['height']; ?>px;
                                <?php endif; ?>
                            }
                        }
                    <?php endif; ?>
                <?php endif; ?>

                <?php if (!empty($logo_mobile_nav)): ?>
                    .mobile-nav__logo {
                        background-size: initial;
                        background-image: url(<?php echo $logo_mobile_nav['url']; ?>);
                        <?php if ($logo_mobile_nav['mime_type'] != 'image/svg+xml'): ?>
                            height: <?php echo $logo_mobile_nav['height']; ?>px;
                            width: <?php echo $logo_mobile_nav['width']; ?>px;
                        <?php endif; ?>
                    }

                    <?php if (!empty($logo_mobile_nav_retina)): ?>
                        @media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {
                            .mobile-nav__logo {
                                background-image: url(<?php echo $logo_mobile_nav_retina['url']; ?>);
                                <?php if ($logo_mobile_nav['mime_type'] != 'image/svg+xml'): ?>
                                    background-size: <?php echo $logo_mobile_nav['width']; ?>px <?php echo $logo_mobile_nav['height']; ?>px;
                                <?php endif; ?>
                            }
                        }
                    <?php endif; ?>
                <?php endif; ?>

                <?php if (!empty($header_menu)): ?>
                    .main-menu>ul>li>a {
                        color: <?php echo $header_menu; ?>;
                    }

                    .main-menu>ul>li>a:hover, .main-menu>ul>li>a:focus {
                        color: <?php echo $header_menu; ?>;
                    }

                    .main-menu>ul>li>a::after {
                        background: <?php echo $header_menu; ?>;
                    }

                    .header--scroll .header__main-menu>ul>li:not(.main-menu__list-item--button)>a {
                        color: <?php echo $header_menu; ?>;
                    }

                    .header--scroll .header__main-menu>ul>li:not(.main-menu__list-item--button)>a:hover, .header--scroll .header__main-menu>ul>li:not(.main-menu__list-item--button)>a:focus {
                        color: <?php echo $header_menu; ?>;
                    }

                    .header--scroll .header__main-menu>ul>li:not(.main-menu__list-item--button)>a::after {
                        background: <?php echo $header_menu; ?>;
                    }
                <?php endif; ?>
            </style>
        <?php endif; ?>

        <?php
            // Blocks > Eyecatcher
            $eyecatcher = get_field('blocks_eyecatcher', 'option');
        ?>
        <?php if (!empty($eyecatcher['heading']) || !empty($eyecatcher['subheading']) || !empty($eyecatcher['text']) || !empty($eyecatcher['carousel'])): ?>
            <style type="text/css">
                <?php if (!empty($eyecatcher['heading'])): ?>
                    .eyecatcher .carousel-caption h1, .eyecatcher .carousel-caption .h1 {
                        color: <?php echo $eyecatcher['heading']; ?>;
                    }
                <?php endif; ?>

                <?php if (!empty($eyecatcher['subheading'])): ?>
                    .eyecatcher .carousel-caption h2, .eyecatcher .carousel-caption .h2 {
                        color: <?php echo $eyecatcher['subheading']; ?>;
                    }
                <?php endif; ?>

                <?php if (!empty($eyecatcher['text'])): ?>
                    .eyecatcher .carousel-caption p {
                        color: <?php echo $eyecatcher['text']; ?>;
                    }
                <?php endif; ?>

                <?php if (!empty($eyecatcher['carousel']['control'])): ?>
                    .eyecatcher .carousel-control-prev-icon, .eyecatcher .carousel-control-next-icon {
                        color: <?php echo $eyecatcher['carousel']['control']; ?>;
                    }
                <?php endif; ?>

                <?php if (!empty($eyecatcher['carousel']['control_hover'])): ?>
                    .eyecatcher .carousel-control-prev:hover .carousel-control-prev-icon, .eyecatcher .carousel-control-next:hover .carousel-control-next-icon {
                        color: <?php echo $eyecatcher['carousel']['control_hover']; ?>;
                    }
                <?php endif; ?>
            </style>
        <?php endif; ?>

        <?php
            // Blocks > Teaser
            $teaser = get_field('blocks_teaser', 'option');
        ?>
        <?php if (!empty($teaser['text'])): ?>
            <style type="text/css">
                <?php if (!empty($teaser['text'])): ?>
                    .teaser__text-wrapper {
                        color: <?php echo $teaser['text']; ?>;
                    }
                <?php endif; ?>
            </style>
        <?php endif; ?>

        <?php
            // Blocks > Benefits
            $benefits = get_field('blocks_benefits', 'option');
        ?>
        <?php if (!empty($benefits['heading']) || !empty($benefits['subheading']) || !empty($benefits['box'])): ?>
            <style type="text/css">
                <?php if (!empty($benefits['heading'])): ?>
                    .benefits__heading {
                        color: <?php echo $benefits['heading']; ?>;
                    }
                <?php endif; ?>

                <?php if (!empty($benefits['subheading'])): ?>
                    .benefits__subheading {
                        color: <?php echo $benefits['subheading']; ?>;
                    }
                <?php endif; ?>

                <?php if (!empty($benefits['box']['icon_background'])): ?>
                    .benefits__box-image-wrapper {
                        background-color: <?php echo $benefits['box']['icon_background']; ?>;
                    }
                <?php endif; ?>

                <?php if (!empty($benefits['box']['border'])): ?>
                    .benefits__box {
                        box-shadow: 0 0 0 4px <?php echo $benefits['box']['border']; ?>;
                    }
                <?php endif; ?>
            </style>
        <?php endif; ?>

        <?php
            // Blocks > Process Carousel
            $process_carousel = get_field('blocks_process_carousel', 'option');
        ?>
        <?php if (!empty($process_carousel['background']) || !empty($process_carousel['heading']) || !empty($process_carousel['subheading']) || !empty($process_carousel['carousel'])): ?>
            <style type="text/css">
                <?php if (!empty($process_carousel['background'])): ?>
                    .process-carousel {
                        background-image: url(<?php echo $process_carousel['background']['url']; ?>);
                    }
                <?php endif; ?>

                <?php if (!empty($process_carousel['heading'])): ?>
                    .process-carousel__heading {
                        color: <?php echo $process_carousel['heading']; ?>;
                    }
                <?php endif; ?>

                <?php if (!empty($process_carousel['subheading'])): ?>
                    .process-carousel__subheading {
                        color: <?php echo $process_carousel['subheading']; ?>;
                    }
                <?php endif; ?>

                <?php if (!empty($process_carousel['carousel']['control'])): ?>
                    .process-carousel__carousel .carousel-control-prev-icon, .process-carousel__carousel .carousel-control-next-icon {
                        color: <?php echo $process_carousel['carousel']['control']; ?>;
                    }
                <?php endif; ?>

                <?php if (!empty($process_carousel['carousel']['control_hover'])): ?>
                    .process-carousel__carousel .carousel-control-prev:hover .carousel-control-prev-icon, .process-carousel__carousel .carousel-control-next:hover .carousel-control-next-icon {
                        color: <?php echo $process_carousel['carousel']['control_hover']; ?>;
                    }
                <?php endif; ?>

                <?php if (!empty($process_carousel['carousel']['active_indicator'])): ?>
                    .process-carousel__carousel .carousel-indicators li.active::after {
                        background: <?php echo $process_carousel['carousel']['active_indicator']; ?>;
                    }
                <?php endif; ?>

                <?php if (!empty($process_carousel['carousel']['active_indicator_ring'])): ?>
                    .process-carousel__carousel .carousel-indicators li.active::after {
                        border-color: <?php echo $process_carousel['carousel']['active_indicator_ring']; ?>;
                    }
                <?php endif; ?>
            </style>
        <?php endif; ?>

        <?php
            // Blocks > Partners
            $partners = get_field('blocks_partners', 'option');
        ?>
        <?php if (!empty($partners['heading']) || !empty($partners['subheading']) || !empty($partners['cta'])): ?>
            <style type="text/css">
                <?php if (!empty($partners['heading'])): ?>
                    .partners__heading {
                        color: <?php echo $partners['heading']; ?>;
                    }
                <?php endif; ?>

                <?php if (!empty($partners['subheading'])): ?>
                    .partners__subheading {
                        color: <?php echo $partners['subheading']; ?>;
                    }
                <?php endif; ?>

                <?php if (!empty($partners['cta']['heading'])): ?>
                    .partners__cta-heading {
                        color: <?php echo $partners['cta']['heading']; ?>;
                    }
                <?php endif; ?>

                <?php if (!empty($partners['cta']['text'])): ?>
                    .partners__cta-text {
                        color: <?php echo $partners['cta']['text']; ?>;
                    }
                <?php endif; ?>
            </style>
        <?php endif; ?>

        <?php
            // Blocks > CTA Application
            $cta_application = get_field('blocks_cta_application', 'option');
        ?>
        <?php if (!empty($cta_application['heading']) || !empty($cta_application['text'])): ?>
            <style type="text/css">
                <?php if (!empty($cta_application['heading'])): ?>
                    .cta-application-text__heading {
                        color: <?php echo $cta_application['heading']; ?>;
                    }
                <?php endif; ?>

                <?php if (!empty($cta_application['text'])): ?>
                    .cta-application-text__content,
                    .cta-application-button__content {
                        color: <?php echo $cta_application['text']; ?>;
                    }
                <?php endif; ?>
            </style>
        <?php endif; ?>

        <?php
            // Blocks > Benefits 2
            $benefits_2 = get_field('blocks_benefits_2', 'option');
        ?>
        <?php if (!empty($benefits_2['heading']) || !empty($benefits_2['subheading'])): ?>
            <style type="text/css">
                <?php if (!empty($benefits_2['heading'])): ?>
                    .benefits-2__heading {
                        color: <?php echo $benefits_2['heading']; ?>;
                    }
                <?php endif; ?>

                <?php if (!empty($benefits_2['subheading'])): ?>
                    .benefits-2__subheading {
                        color: <?php echo $benefits_2['subheading']; ?>;
                    }
                <?php endif; ?>
            </style>
        <?php endif; ?>

        <?php
            // Blocks > Testimonials
            $testimonials = get_field('blocks_testimonials', 'option');
        ?>
        <?php if (!empty($testimonials['background']) || !empty($testimonials['heading']) || !empty($testimonials['subheading']) || !empty($testimonials['carousel'])): ?>
            <style type="text/css">
                <?php if (!empty($testimonials['background'])): ?>
                    .testimonials {
                        background-image: url(<?php echo $testimonials['background']['url']; ?>);
                    }
                <?php endif; ?>

                <?php if (!empty($testimonials['heading'])): ?>
                    .testimonials__heading {
                        color: <?php echo $testimonials['heading']; ?>;
                    }
                <?php endif; ?>

                <?php if (!empty($testimonials['subheading'])): ?>
                    .testimonials__subheading {
                        color: <?php echo $testimonials['subheading']; ?>;
                    }
                <?php endif; ?>

                <?php if (!empty($testimonials['carousel']['control'])): ?>
                    .testimonials__carousel .carousel-control-prev-icon, .testimonials__carousel .carousel-control-next-icon {
                        color: <?php echo $testimonials['carousel']['control']; ?>;
                    }
                <?php endif; ?>

                <?php if (!empty($testimonials['carousel']['control_hover'])): ?>
                    .testimonials__carousel .carousel-control-prev:hover .carousel-control-prev-icon, .testimonials__carousel .carousel-control-next:hover .carousel-control-next-icon {
                        color: <?php echo $testimonials['carousel']['control_hover']; ?>;
                    }
                <?php endif; ?>

                <?php if (!empty($testimonials['carousel']['indicator'])): ?>
                    .testimonials__carousel .carousel-indicators li {
                        background-color: <?php echo $testimonials['carousel']['indicator']; ?>;
                    }
                <?php endif; ?>

                <?php if (!empty($testimonials['carousel']['indicator_active'])): ?>
                    .testimonials__carousel .carousel-indicators .active {
                        background: <?php echo $testimonials['carousel']['indicator_active']; ?>;
                    }
                <?php endif; ?>
            </style>
        <?php endif; ?>

        <?php
            // Blocks > Become Partner
            $become_partner = get_field('blocks_become_partner', 'option');
        ?>
        <?php if (!empty($become_partner['heading']) || !empty($become_partner['subheading'])): ?>
            <style type="text/css">
                <?php if (!empty($become_partner['heading'])): ?>
                    .become-partner__heading {
                        color: <?php echo $become_partner['heading']; ?>;
                    }
                <?php endif; ?>

                <?php if (!empty($become_partner['subheading'])): ?>
                    .become-partner__subheading {
                        color: <?php echo $become_partner['subheading']; ?>;
                    }
                <?php endif; ?>
            </style>
        <?php endif; ?>

        <?php
            // Blocks > CTA Partner
            $cta_partner = get_field('blocks_cta_partner', 'option');
        ?>
        <?php if (!empty($cta_partner['heading'])): ?>
            <style type="text/css">
                <?php if (!empty($cta_partner['heading'])): ?>
                    .cta-partner__heading {
                        color: <?php echo $cta_partner['heading']; ?>;
                    }
                <?php endif; ?>
            </style>
        <?php endif; ?>

        <?php
            // Blocks > Text
            $text = get_field('blocks_text', 'option');
        ?>
        <?php if (!empty($text['heading']) || !empty($text['subheading'])): ?>
            <style type="text/css">
                <?php if (!empty($text['heading'])): ?>
                    .text__heading {
                        color: <?php echo $text['heading']; ?>;
                    }
                <?php endif; ?>

                <?php if (!empty($text['subheading'])): ?>
                    .text__subheading {
                        color: <?php echo $text['subheading']; ?>;
                    }
                <?php endif; ?>
            </style>
        <?php endif; ?>

        <?php
            // Blocks > Pitches
            $pitches = get_field('blocks_pitches', 'option');
        ?>
        <?php if (!empty($pitches['heading']) || !empty($pitches['subheading'])): ?>
            <style type="text/css">
                <?php if (!empty($pitches['heading'])): ?>
                    .pitches__heading {
                        color: <?php echo $pitches['heading']; ?>;
                    }
                <?php endif; ?>

                <?php if (!empty($pitches['subheading'])): ?>
                    .pitches__subheading {
                        color: <?php echo $pitches['subheading']; ?>;
                    }
                <?php endif; ?>
            </style>
        <?php endif; ?>

        <?php
            // Blocks > Mentors
            $mentors = get_field('blocks_mentors', 'option');
        ?>
        <?php if (!empty($mentors['heading']) || !empty($mentors['subheading'])): ?>
            <style type="text/css">
                <?php if (!empty($mentors['heading'])): ?>
                    .mentors__heading {
                        color: <?php echo $mentors['heading']; ?>;
                    }
                <?php endif; ?>

                <?php if (!empty($mentors['subheading'])): ?>
                    .mentors__subheading {
                        color: <?php echo $mentors['subheading']; ?>;
                    }
                <?php endif; ?>
            </style>
        <?php endif; ?>

        <?php
            // Blocks > Process
            $process = get_field('blocks_process', 'option');
        ?>
        <?php if (!empty($process['heading']) || !empty($process['subheading'])): ?>
            <style type="text/css">
                <?php if (!empty($process['heading'])): ?>
                    .process__heading {
                        color: <?php echo $process['heading']; ?>;
                    }
                <?php endif; ?>

                <?php if (!empty($process['subheading'])): ?>
                    .process__subheading {
                        color: <?php echo $process['subheading']; ?>;
                    }
                <?php endif; ?>
            </style>
        <?php endif; ?>

        <?php
            // Blocks > FAQ
            $faq = get_field('blocks_faq', 'option');
        ?>
        <?php if (!empty($faq['background']) || !empty($faq['heading']) || !empty($faq['item'])): ?>
            <style type="text/css">
                <?php if (!empty($faq['background'])): ?>
                    div.faq {
                        background-image: url(<?php echo $faq['background']['url']; ?>);
                    }
                <?php endif; ?>

                <?php if (!empty($faq['heading'])): ?>
                    .faq__heading {
                        color: <?php echo $faq['heading']; ?>;
                    }
                <?php endif; ?>

                <?php if (!empty($faq['item']['question'])): ?>
                    div.faq .accordion-item__question {
                        color: <?php echo $faq['item']['question']; ?>;
                    }
                <?php endif; ?>
            </style>
        <?php endif; ?>

        <?php
            // Blocks > Heading
            $heading = get_field('blocks_heading', 'option');
        ?>
        <?php if (!empty($heading['heading']) || !empty($heading['subheading'])): ?>
            <style type="text/css">
                <?php if (!empty($heading['heading'])): ?>
                    .heading__heading {
                        color: <?php echo $heading['heading']; ?>;
                    }
                <?php endif; ?>

                <?php if (!empty($heading['subheading'])): ?>
                    .heading__subheading {
                        color: <?php echo $heading['subheading']; ?>;
                    }
                <?php endif; ?>
            </style>
        <?php endif; ?>

        <?php
            // Blocks > Contact
            $contact = get_field('blocks_contact', 'option');
        ?>
        <?php if (!empty($contact['heading'])): ?>
            <style type="text/css">
                <?php if (!empty($contact['heading'])): ?>
                    .contact-text__heading,
                    .contact-people__heading {
                        color: <?php echo $contact['heading']; ?>;
                    }
                <?php endif; ?>
            </style>
        <?php endif; ?>

        <?php
            // Footer
            $by_upstart = get_field('images_by_upstart', 'option');
            $by_upstart_retina = get_field('images_by_upstart_retina', 'option');
        ?>
        <?php if (!empty($by_upstart)): ?>
            <style type="text/css">
                <?php if (!empty($by_upstart)): ?>
                    .footer__text-by {
                        background-size: initial;
                        background-image: url(<?php echo $by_upstart['url']; ?>);
                        <?php if ($by_upstart['mime_type'] != 'image/svg+xml'): ?>
                            height: <?php echo $by_upstart['height']; ?>px;
                            width: <?php echo $by_upstart['width']; ?>px;
                        <?php endif; ?>
                    }

                    <?php if (!empty($by_upstart_retina)): ?>
                        @media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {
                            .footer__text-by {
                                background-image: url(<?php echo $by_upstart_retina['url']; ?>);
                                <?php if ($by_upstart['mime_type'] != 'image/svg+xml'): ?>
                                    background-size: <?php echo $by_upstart['width']; ?>px <?php echo $by_upstart['height']; ?>px;
                                <?php endif; ?>
                            }
                        }
                    <?php endif; ?>
                <?php endif; ?>
            </style>
        <?php endif; ?>

        <?php
            // Additional CSS
            $additional_css = get_field('additional_css', 'option');
        ?>
        <?php if (!empty($additional_css)): ?>
            <style type="text/css">
                <?php echo $additional_css; ?>
            </style>
        <?php endif; ?>

        <?php echo get_field('before_the_closing_head_tag', 'option'); ?>

    </head>
	<body <?php body_class(); ?> itemscope itemtype="https://schema.org/WebPage">

        <?php echo get_field('after_the_opening_body_tag', 'option'); ?>

        <div id="page">

            <header class="header" role="banner" itemscope itemtype="https://schema.org/WPHeader">
                <div class="header__container">
                    <div class="header__logo-wrapper" itemscope itemtype="https://schema.org/Organization">
                        <a href="<?php echo get_current_page_home_url(); ?>" rel="nofollow" itemprop="url" title="<?php bloginfo('name'); ?>" class="logo header__logo"><?php bloginfo('name'); ?></a>
                    </div>

                    <div class="header__menus">
                        <?php if (has_nav_menu('top-nav'.get_current_page_language('-'))): ?>
                            <nav class="top-menu header__top-menu" role="navigation" itemscope itemtype="https://schema.org/SiteNavigationElement">
                                <?php // see all default args here: https://developer.wordpress.org/reference/functions/wp_nav_menu/ ?>
                                <?php wp_nav_menu(array(
                                    'menu' => __('Top Menu', 'platetheme'),
                                    'menu_class' => '',
                                    'container' => false,
                                    'container_class' => '',
                                    'depth' => 1,
                                    'theme_location' => 'top-nav'.get_current_page_language('-'),
                                )); ?>
                            </nav>
                        <?php endif; ?>

                        <nav class="main-menu header__main-menu" role="navigation" itemscope itemtype="https://schema.org/SiteNavigationElement">
                            <?php // see all default args here: https://developer.wordpress.org/reference/functions/wp_nav_menu/ ?>
                            <?php wp_nav_menu(array(
                                'menu' => __('Main Menu', 'platetheme'),
                                'menu_class' => '',
                                'container' => false,
                                'container_class' => '',
                                'depth' => 1,
                                'theme_location' => 'main-nav'.get_current_page_language('-'),
                            )); ?>
                        </nav>

                        <nav class="language-menu header__language-menu">
                            <?php // see all default args here: https://developer.wordpress.org/reference/functions/wp_nav_menu/ ?>
                            <?php wp_nav_menu(array(
                                'menu' => __('Language Menu', 'platetheme'),
                                'menu_class' => '',
                                'container' => false,
                                'container_class' => '',
                                'depth' => 1,
                                'theme_location' => 'language-nav',
                            )); ?>
                        </nav>
                    </div>
                </div>
            </header>

            <input type="checkbox" id="hamburger" class="mobile-nav-checkbox">
            <label for="hamburger" class="mobile-nav-button" onclick="toggleMobileMenu();">
                <span class="mobile-nav-button__hamburger"></span>
                <span class="sr-only">Menü</span>
            </label>
            <div class="mobile-nav">
                <div class="mobile-nav__header">
                    <a href="<?php echo get_current_page_home_url(); ?>" rel="nofollow" itemprop="url" title="<?php bloginfo('name'); ?>" class="logo mobile-nav__logo"><?php bloginfo('name'); ?></a>
                </div>
                <div class="mobile-nav__content">
                    <?php // see all default args here: https://developer.wordpress.org/reference/functions/wp_nav_menu/ ?>
                    <?php wp_nav_menu(array(
                        'menu' => __('Main Menu', 'platetheme'),
                        'menu_class' => '',
                        'container' => false,
                        'container_class' => '',
                        'depth' => 1,
                        'theme_location' => 'main-nav'.get_current_page_language('-'),
                    )); ?>
                </div>
                <div class="mobile-nav__footer">
                    <?php // see all default args here: https://developer.wordpress.org/reference/functions/wp_nav_menu/ ?>
                    <?php wp_nav_menu(array(
                        'menu' => __('Language Menu', 'platetheme'),
                        'menu_class' => '',
                        'container' => false,
                        'container_class' => '',
                        'depth' => 1,
                        'theme_location' => 'language-nav',
                    )); ?>
                </div>
            </div>

            <div class="main">
